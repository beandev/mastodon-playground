import os.path
from opensearchpy import OpenSearch

from oshelper import OpenSearchHelper
from oshelper import Migration
from oshelper import OpenSearchBackup

os_client: OpenSearch = OpenSearchHelper.os_open(config_yaml="oscluster-config.yaml")

# Standard migration:
MIGRATION_PATH = os.path.abspath("./os/migration/")


def _list_index_alias_from_migration(dir_name: str) -> [str]:
    """
    Lists all well known index-aliases in the migration path.

    :param path: the path to read the migration files to list the indexes to backup later
    :return: a list of indexes, managed by the migration
    """
    indexes = []
    for migration_file in Migration.list_migration_files(dir_name=dir_name):
        migration = Migration.load_migration(dir_name=dir_name, file_name=migration_file)
        for r in migration['run']:
            if r['call'] == 'create index':
                index = r['index_name']
                if index not in indexes:
                    indexes.append(index)
    return indexes


def _delete_all_versions (client: OpenSearch, index_alias: str):
    # Remove the alias from all indexes
    if client.indices.exists_alias(name=index_alias):
        client.indices.delete_alias(index="*", name=index_alias)
    client.indices.delete(index=index_alias + "_v*", ignore_unavailable=True, expand_wildcards="all")


def reset_migration (client: OpenSearch, dir_name: str):
    """
    This function deletes the whole migrated set to restart a migration.
    Caution: This will purge all the data, related to the migration scripts in dir_name.
    :param client: OpenSearch cluster client-access

    """
    Migration.migration_history_delete(client)
    _delete_all_versions(client, Migration.MIGRATION_HISTORY)

    for alias in _list_index_alias_from_migration(dir_name):
        _delete_all_versions(client, alias)


# before reset, we should make a backup:
OpenSearchBackup.backup(os_client, indexes_from=MIGRATION_PATH)

# This will purge the whole data stored into the migration-indices, be careful:
reset_migration(os_client, MIGRATION_PATH)