import os.path
from opensearchpy import OpenSearch

from oshelper import OpenSearchHelper
from oshelper import OpenSearchBackup
from oshelper import Migration
from oshelper import MigrationDev

client: OpenSearch = OpenSearchHelper.os_open(config_yaml="oscluster-config.yaml")

# Standard migration:
path = os.path.abspath("./os/migration/")
Migration.migration(client, path)


# #############################################################################
# The following methods are maybe useful for managing different index versions:

# Some new OpenSearchHelper methods:
# ##################################

# # Example to copy an index (e.g. for archiving):
# OpenSearchHelper.os_copy_index(client, "toots", "toots_arch-1")

# # Example to rename an index:
# OpenSearchHelper.os_rename_index(client, "toots_arch-1", "toots_arch-2")


# Some new MigrationDev helper methods:
# #####################################

# # Example for an undo operation. Please be careful. Only useful to re-test migrations or undo issues.
# # Can purge your data
# MigrationDev.undo(client, dir_name=path, to_version='1.3', archive=True)
# MigrationDev.undo(client, dir_name='./os/migration_history', to_version='0.0', archive=False, delete=True)

# Migration.switch_version(client, index_alias="migration_history", version='0.0', close_index="migration_history_v1_0")
# client.indices.delete(index="migration_history_v1_0", ignore_unavailable=True)

