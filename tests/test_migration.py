import unittest

from packaging.version import Version

from oshelper import Migration


class MigrationTestCase(unittest.TestCase):
    def test_listfiles_filtered(self):
        self.assertTrue(len(Migration.list_migration_files('./os/migration/', version_end=Version('1.1'))) == 1,
                        "We should have only one file")

    def test_listfiles_sort(self):
        files = Migration.list_migration_files('./os/migration/', version_end=Version('1.4'))

        self.assertEqual(files[0], "V01.001__CreateTemplate.json")
        self.assertEqual(files[3], "V01.004__MigrateTootsIndex.json")


if __name__ == '__main__':
    unittest.main()
