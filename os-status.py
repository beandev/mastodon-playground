import json
import oshelper.OpenSearchHelper
from opensearchpy import OpenSearch

client: OpenSearch = oshelper.OpenSearchHelper.os_open(config_yaml="oscluster-config.yaml")

indexes = client.indices.get(index="*")

print(json.dumps(indexes, indent=4, default=str))

