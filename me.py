import json

import mastobot.MastodonHelper

"""
This script, shows how to use MastodonHelper to log in directly under a specific account and output the account data.
"""


# Open a connection to the mastodon instance and register this script with a name (if needed)
# Start a login process (this means a Terminal-Login asks for the account e-mail address and password).
# All the registered application data and the access token from the login will be stored in the HOME folder.
# When the script is started again, the saved information is reused and the login credentials are not asked again.
mastodon = mastobot.MastodonHelper.open_or_create_app(
    config_yaml='mastobot-config.yaml',
    login=True
)

# Request my account data:
me = mastodon.me()

# Print out basic stuff:
print("\n\n")
print(f"Me: {me['id']} = {me['username']}")

# Print out everything:
print(json.dumps(me, indent=4, default=str))
