import collections
import getpass
import hashlib
import json
import os
import re
import sys
import time
from datetime import datetime
from json import JSONDecodeError
from packaging.version import Version

from . import OpenSearchHelper

from opensearchpy import OpenSearch

# Migration History index name
MIGRATION_HISTORY = "migration_history"

_VERSION_MAX = Version('9999999999')

# Pattern to detect migration file and extract in group 1 the version and in group 2 the remark
MIGRATION_FILE_PATTERN = re.compile(r"^V([0-9.]+)__(.*)\.(json)$", re.IGNORECASE)

_VERSIONED_INDEX_PATTERN = re.compile(r"^([a-z0-9].*)_v([0-9_]+)$")

# Simple pattern to detect regular expressions in groovy
GROOVY_REGEXP_PATTERN = re.compile(r"(\s*def\s+.*\s*=\s*)(~')(.*)(')(.*)")

GROOVY_END_SEMI_PATTERN = re.compile(r'.*;\s*$')
GROOVY_END_PATTERN = re.compile(r'.*[{\[(\]}]\s*$')


def md5(file: str):
    """
    Calculates a md5 hash for a file.
    :param file: absolute path
    :return: hex digest
    """
    hash_md5 = hashlib.md5()
    with open(file, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def list_migration_files(dir_name: str, version_end=_VERSION_MAX) -> [str]:
    """
    Loads all migration files from a directory (without sub-dirs)
    and return the files in a sorted list by version number.

    :param dir_name: the folder where we search the files
    :param version_end: look up to this version
    :return: a list of filenames
    """
    return sorted(
        filter(
            lambda x: bool(MIGRATION_FILE_PATTERN.match(x))
                      and Version(MIGRATION_FILE_PATTERN.match(x).group(1)) <= version_end
                      and os.path.isfile(os.path.join(dir_name, x)), os.listdir(dir_name)
        ), key=lambda x: Version(MIGRATION_FILE_PATTERN.match(x).group(1))
    )


def _inject_migration_meta(dir_name: str, file_name: str, script: dict):
    """
    Extracts filename information and passes this data
    to the script content to log it later in migration_history.
    :param dir_name: folder of the script
    :param file_name: filename of the script
    :param script: the script content as dict
    """
    abs_path = os.path.join(dir_name, file_name)
    checksum = md5(abs_path)
    match = MIGRATION_FILE_PATTERN.match(file_name)
    version = match.group(1)
    description = match.group(2)
    script_type = match.group(3).upper()
    script['__self'] = {}
    script['__self']['path'] = abs_path
    script['__self']['checksum'] = checksum
    script['__self']['version'] = str(Version(version))
    script['__self']['description'] = description
    script['__self']['type'] = script_type


def load_migration(dir_name: str, file_name: str):
    """
    Loads the JSON file into a dict.
    :param dir_name: path to the file
    :param file_name: file name
    :return: dict
    """
    abs_path = os.path.join(dir_name, file_name)
    try:
        with open(os.path.join(dir_name, file_name), "r") as stream:
            d = json.load(stream)
            # Inject the source file path for better error handling
            _inject_migration_meta(dir_name, file_name, d)
            return d
    except JSONDecodeError:
        print(f"JSON parse error in {abs_path}", file=sys.stderr)
        raise


def _load_plain(source_path: str, file_name: str):
    payload_file = os.path.join(
        os.path.dirname(os.path.abspath(source_path)),
        os.path.basename(file_name.removeprefix("plain://"))
    )
    inline = None
    if os.path.exists(payload_file):
        with open(payload_file, "r", encoding="utf-8") as stream:
            inline = stream.read()
    return inline


def groovy_to_painless(multi_line: str) -> str:
    ret = ""
    for line in multi_line.splitlines():
        ret += _groovy_to_painless(line)
    return ret


def _groovy_to_painless(line: str) -> str:
    """
    Converts some groovy like statements and operations to the special painless syntax.

    * Converts ~'<regex>' to /<regex>/

      Please note, a def without the type 'Pattern' is needed. E.g. def o = ~'regex';

    * appends ';' on a line, if missing and we have not an open parenthesis '([{'

    :param line:
    :return:
    """
    if not line or not line.strip():
        return line

    line = line.rstrip('\n')

    rm = GROOVY_REGEXP_PATTERN.match(line)
    if rm:
        line = rm.group(1) + "/" + rm.group(3).replace('\\\\', '\\') + "/" + rm.group(5)

    if not GROOVY_END_SEMI_PATTERN.match(line):
        if not GROOVY_END_PATTERN.match(line):
            line += ";"

    return line + '\n'


def _load_inline_script(source_path: str, file_name: str):
    payload_file = os.path.join(
        os.path.dirname(os.path.abspath(source_path)),
        os.path.basename(file_name.removeprefix("script://"))
    )

    _, ext = os.path.splitext(file_name)

    groovy = ext.lower() == '.groovy'

    script = ""
    if os.path.exists(payload_file):
        script_started = False
        guess_unsused = ""
        with open(payload_file, "r", encoding="utf-8") as stream:
            lines = stream.readlines()
            for line in lines:
                if line.startswith("//!script"):
                    script_started = True
                    continue
                if line.startswith("//!endscript"):
                    break

                if groovy:
                    line = _groovy_to_painless(line)

                if script_started:
                    script += line
                else:
                    guess_unsused += line
        if not script_started:
            # The script is not inside
            # //!script
            # //!scriptend
            # and this means, everything is the script:
            script = guess_unsused
    return script


def _load_recurse_inline(migration_script, source_path: str, param):
    for key, value in param.items():
        if isinstance(value, str):
            if value.startswith("script://"):
                inline = _load_inline_script(source_path, value)
                if inline:
                    param[key] = inline
            elif value.startswith("plain://"):
                inline = _load_plain(source_path, value)
                if inline:
                    param[key] = inline
        elif isinstance(value, collections.Mapping):
            return _load_recurse_inline(migration_script, source_path, value)


def _load_migration_payload(migration_script, run: dict, param: str):
    """
    Loads for the param key in a run-object the data from the payloads section or file-system.
    A file-system load is useful for multiline content (e.g. painless scripts)
    :param migration_script: The migration script as a dict
    :param param: the name of the run parameter to load the content
    """
    source_path = migration_script['__self']['path']

    if param in run and isinstance(run[param], str):
        endpoint = run['call']

        payload_key: str = run[param]
        # Check if the name is inside the payloads
        if 'payloads' not in migration_script:
            raise ValueError(f"Missing payloads, but endpoint {endpoint}.{param} needs {payload_key} "
                             f"in migration script {source_path}")
        if payload_key not in migration_script['payloads']:
            raise ValueError(f"Missing payload {payload_key} in payloads, "
                             f"but endpoint {endpoint}.{param} needs this content "
                             f"in migration script {source_path}")
        # We found it and we copy it over the reference
        run[param] = migration_script['payloads'][payload_key]
    elif param in run and isinstance(run[param], collections.Mapping):
        _load_recurse_inline(migration_script, source_path, run[param])


def _resolve_migration_runners(migration_script):
    """
    Prepares a run object. This means the method resolves a body reference to a payloads content.
    :param migration_script: a migration scripts
    :return: all run items
    """

    if 'run' not in migration_script:
        raise ValueError(f"Missing 'run' in migration script {migration_script['__self']['path']}")
    for run in migration_script['run']:
        # Fetch the source_path for better error messages
        source_path = migration_script['__self']['path']
        if 'call' not in run:
            raise ValueError(f"Missing caller endpoint in migration script {migration_script['__self']['path']}")

        _load_migration_payload(migration_script, run, 'body')
        _load_migration_payload(migration_script, run, 'reindex_body')

        # Inject the source file path for better error handling
        run['__self'] = migration_script['__self']
        yield run


def switch_version(client: OpenSearch, index_alias: str, version=None, index_to=None, close_index=None):
    """
    Moves the index alias name to the index with the name index_alias + '_v' + version.
    The switch_version function currently doesn't work with spanning over different multiple indexes.

    :param client: OpenSearch client
    :param index_alias: the alias without a version number (e.g. 'toots')
    :param version: a two level version Number, e.g. 1.0, 3.1, or 4
    :param index_to: the destination index name, instead of a version
    :param close_index: The name of the Index to close
    :return: response from the alias creation
    """

    if not index_to:
        if version is not None:
            index_to = index_alias + "_v" + str(Version(version)).replace('.', '_')
        else:
            raise ValueError(f"Missing version and index_to. Cannot switch the version for alias '{index_alias}'")

    # Move or creates the alias. The remove works also in cases of non-existing alias.
    move_alias = {
        "actions": [
            {
                "remove": {
                    "index": "*",
                    "alias": index_alias
                }
            },
            {
                "add": {
                    "index": index_to,
                    "alias": index_alias
                }
            }
        ]
    }
    response = client.indices.update_aliases(body=move_alias)
    print(f"Created alias {index_alias} on index {index_to}")

    if close_index:
        _close_index(client, close_index)

    _open_index(client, index_to)

    return response


def _get_current_version_index(client: OpenSearch, index_alias: str) -> str:
    """
    Searches for the current index name for an alias.
    :param client: OpenSearch client
    :param index_alias: Name of the alias
    :return: the reals index name behind the alias
    """
    alias_response = client.indices.get_alias(name=index_alias)
    # The response is a view from index to aliases:
    # {
    #     "toots_v1_002": {
    #         "aliases": { "toots": {} }
    #     }
    # }

    # We hope, we have not a spanning alias, this means only a single result:
    if len(alias_response) != 1:
        raise RuntimeError(f"The alias {index_alias} is a spanning alias over more than one index")

    return next(iter(alias_response.keys()))


def _index_version_find(client: OpenSearch, index_alias: str, version):
    """
    Searches, if an index with the version already exists.
    :param client: OpenSearch client
    :param index_alias: the alias name (without version)
    :param version: the version to search
    :return: the name of the index
    """
    sv = Version(str(version))
    meta = client.cluster.state(metric='metadata', index=index_alias + '_v*', expand_wildcards='all',
                                filter_path='metadata.indices.*.state')
    if meta:
        for index_name in meta['metadata']['indices'].keys():
            iv = Version(_parse_version(index_alias, index_name))
            if iv == sv:
                return index_name
    return None


def _parse_version(index_alias: str, index_name: str):
    """
    Parse the Version String for an Index.
    :param index_alias: Alias Prefix
    :param index_name: The full versioned Index
    :return: the version of the index or None
    """
    match = re.match(index_alias + "_v([0-9_]+)", index_name)
    if match:
        return match.group(1).replace('_', '.')
    return None


def clean_up(client: OpenSearch, index_alias: str, keep_version=None, only_closed=True, simulate=False):
    """
    Removes older indexes.
    :param client: OpenSearch client
    :param index_alias: the Alias name
    :param keep_version: the version to keep (and all higher). None, if we keep the latest
    :param only_closed: delete/hide only the closed indexes
    :param simulate: Set to true, to get only a dump of indexes to delete (without a real delete)
    :return:
    """

    if not client.indices.exists_alias(name=index_alias):
        raise RuntimeError(f"Alias {index_alias} doesn't exists, cannot proceed with clean_up")

    index_current = _get_current_version_index(client, index_alias)
    version_str = _parse_version(index_alias, index_current)
    if not version_str:
        raise RuntimeError(f"Alias {index_alias} is not assigned to a version index {index_current}")

    if not keep_version:
        keep = Version(version_str)
    else:
        keep = Version(str(keep_version))

    keep_found = False
    found_indexes = {}
    meta = client.cluster.state(metric='metadata', index=index_alias + '_v*', expand_wildcards='all',
                                filter_path='metadata.indices.*.state')
    if meta:
        for index_name in meta['metadata']['indices'].keys():
            v = _parse_version(index_alias, index_name)
            if v:
                ver = Version(v)
                if ver == keep:
                    keep_found = True
                elif ver < keep:
                    if only_closed and meta['metadata']['indices'][index_name]['state'] == 'open':
                        continue
                    found_indexes[Version(v)] = index_name

    if not keep_found:
        raise RuntimeError(f"Can't find the version '{keep_version}' to keep")

    for version, index_name in found_indexes.items():
        if not simulate:
            client.indices.delete(index=index_name)
        print(f"Version {version} of '{index_alias}', index '{index_name}' "
              f"{'deleted' if not simulate else 'is a candidate to delete.'}.")


def _close_index(client: OpenSearch, index_name: str):
    """
    Closes the index. It's not possible to write or read after close.

    :param client: OpenSearch client
    :param index_name: full index name
    """
    client.indices.close(index=index_name, ignore_unavailable=True)


def _open_index(client: OpenSearch, index_name: str):
    """
    Opens the index. Now it's possible to write and read data.

    :param client: OpenSearch client
    :param index_name: full index name
    """
    client.indices.open(index=index_name)


def _run_create_index(client: OpenSearch, runner):
    """
    Creates a new index and migrates an old index (if existing).

    :param client: OpenSearch client
    :param runner: The runner structure with all parameters
    :return: the response from the index creation
    """
    index_alias = runner['index_name']
    version = str(runner['__self']['version'])

    index_to_exists = _index_version_find(client, index_alias, version)
    if index_to_exists:
        # we are behind a future version. The history was deleted?
        # Switch to this version and that's it
        # todo: the other index version must be closed
        switch_version(client, index_alias=index_alias, index_to=index_to_exists)
        return

    index_to = index_alias + "_v" + str(Version(version)).replace('.', '_')
    script = None
    if 'reindex_body' in runner and 'script' in runner['reindex_body']:
        script = runner['reindex_body']['script']

    # Now we have some cases to check
    # 1. We have a plain index, named like the alias.
    #    This means we start here with a version, but with existing index to migrate
    # 2. We have not an alias (and maybe not an index to migrate, because we cannot detect the name)
    #    This means, we start from green field with the  first version
    # 3. We have an alias and this alias points to an index with a version postfix
    #    This means, we did already migrations in the past

    index_or_alias_exists: bool = client.indices.exists(index=index_alias, allow_no_indices=False)
    alias_exists: bool = client.indices.exists_alias(name=index_alias)

    if index_or_alias_exists and not alias_exists:
        # Case 1
        response = client.indices.create(index_to, body=runner['body'])
        # Now we cannot switch the version, because the old index with the name equal to alias exists
        # We must copy first the documents
        OpenSearchHelper.os_copy_documents(client, index_from=index_alias, index_to=index_to, script=script)
        # No failures, so we can remove the old index (yes, it's hard)
        client.indices.delete(index=index_alias)
        # Now we can switch the version (this means, we create an alias):
        switch_version(client, index_alias=index_alias, version=version)
    elif not index_or_alias_exists and not alias_exists:
        # Case 2
        response = client.indices.create(index_to, body=runner['body'])
        switch_version(client, index_alias=index_alias, version=version)
    elif index_or_alias_exists and alias_exists:
        # Case 3
        # We need the index behind the alias name to migrate the documents
        index_from = _get_current_version_index(client, index_alias)

        if index_from != index_to:
            response = client.indices.create(index_to, body=runner['body'])
            OpenSearchHelper.os_copy_documents(client, index_from=index_from, index_to=index_to, script=script)
            switch_version(client, index_alias=index_alias, version=version, close_index=index_from)
        else:
            response = {}
            print(f"Warning: Existing index {index_from} is equal to the "
                  f"migration version. Maybe the history was deleted?")
    else:
        raise RuntimeError("No idea, what to do")

    return response


def _run_create_index_template(client: OpenSearch, runner):
    """
    Creates or updates the index template.

    :param client: OpenSearch client
    :param runner: The runner structure with all parameters
    :return: the response from the template creation
    """
    return client.indices.put_index_template(runner['template_name'], body=runner['body'])


_CALLBACK_RUNNERS = {
    "create index": _run_create_index,
    "create index template": _run_create_index_template
}


def _run_migration_runner(client: OpenSearch, runner):
    endpoint = runner['call']
    if endpoint in _CALLBACK_RUNNERS:
        _CALLBACK_RUNNERS[endpoint](client, runner)
    else:
        raise ValueError(f"Unknown command {endpoint} in script {runner['__self']['path']}")


def _parse_file_version(file_name: str) -> Version:
    """
    Extract the version from a filename.
    :param file_name: the filename
    :return: the Version object
    """
    return Version(MIGRATION_FILE_PATTERN.match(file_name).group(1))


def migration_history_prepare(client: OpenSearch, to_internal_version=None):
    """
    Creates the migration_history index for our history of scripts and updates to the latest version.
    :param client: OpenSearch client
    :param to_internal_version: stop migration at this version
    """

    version_start = None
    version_end = Version(to_internal_version) if to_internal_version else _VERSION_MAX

    # We cannot use the history, we fetch the index version from the cluster
    if client.indices.exists(index=MIGRATION_HISTORY):
        migration_index_info = client.indices.get(index=MIGRATION_HISTORY)
        migration_index_name = next(iter(migration_index_info.keys()))
        if migration_index_name != MIGRATION_HISTORY:
            # We are over version 0
            version_start = Version(_parse_version(MIGRATION_HISTORY, migration_index_name))
        else:
            # We are on version 00.000 (because the version 0 was a plain index name)
            version_start = Version('0')

    path = "./os/migration_history"
    for file in list_migration_files(path):
        file_version = _parse_file_version(file)
        if (version_start is None or file_version > version_start) and (file_version <= version_end):
            prepare = load_migration(path, file)
            for run in _resolve_migration_runners(prepare):
                _run_migration_runner(client, run)


def migration_history_delete(client: OpenSearch):
    """
    Deletes the migration_history index
    :param client: OS client
    """
    if client.indices.exists(index=MIGRATION_HISTORY):
        mi = client.indices.get(index=MIGRATION_HISTORY)

        # Get the real versioned index name:
        mi_name = next(iter(mi.keys()))
        client.indices.delete(index=mi_name)


def _check_migrated(client: OpenSearch, runner) -> (str, bool):
    """
    Checks, if the migration script was already executed in the past.
    :param client: OpenSearch client
    :param runner: the script to check, if it was already migrated
    :return: ID of the found history and success
    """
    version: str = str(Version(str(runner['__self']['version'])))

    search = {
        "size": 1,
        "query": {
            "bool": {
                "filter": [
                    {"term": {"version": version}}
                ]
            }
        }
    }
    response = client.search(body=search, index=MIGRATION_HISTORY)
    hits = response['hits']['hits']
    if hits:
        return hits[0]['_id'], hits[0]['_source']['success']
    else:
        return None, False


def create_history(client, runner, undo_operation=False):
    """
    Creates a new history records with a pending state
    :param client: OpenSearch client
    :param runner: the script to check, if it was already migrated
    :param undo_operation: True, if it's an undo operation
    :return: the record ID
    """
    try:
        user = getpass.getuser()
    except:
        user = None

    history_record = {
        "version": str(Version(str(runner['__self']['version']))),
        "description": ("Undo of " if undo_operation else "") + runner['__self']['description'],
        "type": runner['__self']['type'],
        "script": runner['__self']['path'],
        "checksum": runner['__self']['checksum'],
        "installed_by": user,
        "installed_on": datetime.utcnow().isoformat(),
        "execution_time": 0,
        "success": False
    }
    response = client.index(index='migration_history', body=history_record, refresh=True)
    return response['_id']


def update_history_success(client, duration: float, history_id, message=None):
    """
    Updates the history record with the last information about the run.
    :param client: OpenSearch client
    :param history_id: the id of the history record
    :param message: Success message
    """
    history_result = client.get(index=MIGRATION_HISTORY, id=history_id)
    doc = history_result['_source']
    doc['installed_on'] = datetime.utcnow().isoformat()
    doc['execution_time'] = duration
    doc['success'] = True
    if message:
        doc['message'] = doc.get('message', '') \
                         + '\n\n' + datetime.now().isoformat() + ':\n' + message

    response = client.index(index='migration_history', id=history_id, body=doc, refresh=True)
    return response['result'] == 'updated'


def update_history_error(client, history_id, root_cause: Exception = None, root_message=None):
    """
    Updates the history record and adds an error message.
    :param client: OpenSearch client
    :param history_id: the id of the history record
    :param root_cause: the root cause of the error
    """
    history_result = client.get(index=MIGRATION_HISTORY, id=history_id)
    doc = history_result['_source']

    if root_cause:
        if root_message:
            message = root_message + " " + getattr(root_cause, 'message', repr(root_cause))
        else:
            message = getattr(root_cause, 'message', repr(root_cause))
    elif root_message:
        message = root_message
    else:
        message = "Unknown error"

    doc['message'] = doc.get('message', '') + '\n\n' + datetime.now().isoformat() + ':\n' + message

    response = client.index(index=MIGRATION_HISTORY, id=history_id, body=doc, refresh=True)
    return response['result'] == 'updated'


def migration(client: OpenSearch, dir_name: str):
    """
    The migration runner
    :param client: OpenSearch client
    :param dir_name: folder to load from the migration scripts
    """
    _migration(client, dir_name)


def _migration(client: OpenSearch, dir_name: str, to_internal_version=None, to_migration_version=None):
    """
    The migration runner
    :param client: OpenSearch client
    :param dir_name: folder to load from the migration scripts
    """
    # We check, if the migration_history on the latest version and update it
    migration_history_prepare(client, to_internal_version=to_internal_version)

    # Now the migration
    version_end = Version(to_migration_version) if to_migration_version else _VERSION_MAX
    for file in list_migration_files(dir_name, version_end):
        migration_script = load_migration(dir_name, file)
        history_id, success = _check_migrated(client, migration_script)
        if not success:
            print(f"Run {file}")
            if not history_id:
                history_id = create_history(client, migration_script)
            start_ms = time.time_ns() / 1_000_000
            try:
                for run in _resolve_migration_runners(migration_script):
                    _run_migration_runner(client, run)
            except Exception as e:
                update_history_error(client, history_id, e)
                print("  error")
                break
            else:
                duration = (time.time_ns() / 1_000_000) - start_ms
                update_history_success(client, duration, history_id)
                print("  success")
