import os.path
import re
import time
from packaging.version import Version

from opensearchpy import OpenSearch

from . import Migration
from . import OpenSearchHelper

# Pattern to split a index name from version label
_VERSIONED_INDEX_PATTERN = re.compile(r"^([a-z0-9].*)_v([0-9_]+)$")


def _load_migration_data(dir_name: str, file_name: str, data: dict):
    """
    Loads the migration script file data into a data-structure
    :param dir_name: folder
    :param file_name: script name
    :param data: data structure to fill
    :return: the complete content
    """
    migration_file = Migration.load_migration(dir_name, file_name)
    indexes = []
    for run in migration_file['run']:
        if run['call'] == 'create index':
            indexes.append(run['index_name'])

    data['index_aliases'] = indexes
    return migration_file


def undo(client: OpenSearch, dir_name: str, to_version, delete=False, archive=True):
    """
    Undo a migration and resets to version 'to_version'. With 'delete=true' all indexes
    higher than to_version will be deleted.

    Warning: This is a development method and under construction.

    :param client: OpenSearch client
    :param dir_name: Folder with migration scripts
    :param to_version: destination version
    :param delete: set to True, to really delete the indexes
    :param archive: True, archives the undo-indexes (set to False, if delete=True)
    """

    undo_version = Version(str(to_version))

    undo_point_found = False
    undo_point_script = None

    # Check the History
    migrations = Migration.list_migration_files(dir_name)
    candidates = []
    undo_versions = []
    for migration_file in migrations:
        file_version = Version(Migration.MIGRATION_FILE_PATTERN.match(migration_file).group(1))
        data: dict = {"script": migration_file, "script_version": file_version}
        script = _load_migration_data(dir_name, migration_file, data)

        if file_version <= undo_version:
            if file_version == undo_version:
                undo_point_found = True
                undo_point_script = script
            candidates.append(data)
        else:
            candidates.append(data)
            undo_versions.append(file_version)

    if undo_version > Version('0') and not undo_point_found:
        raise ValueError(f"Can't find the version '{to_version}' as undo point. "
                         f"Use a valid version or '0' to undo everything")

    if len(undo_versions) == 0:
        print(f"Nothing found to undo to version {to_version}. Maybe this is the latest version.")
        return

    # Search indexes on OS cluster prepare a map of versions
    keep_indexes = {}
    delete_indexes = {}
    for candidate in candidates:
        # any script can have one or more lines with create index command. We check, if we have still existing
        # Indexes on our cluster to undo it to the undo-point
        for index_alias in candidate['index_aliases']:
            if index_alias in keep_indexes:
                found_version = keep_indexes[index_alias]['undo_version']
                found_index = keep_indexes[index_alias]['undo_index']
            else:
                found_version = Version('0')
                found_index = None
            meta = client.cluster.state(metric='metadata', index=index_alias + '_v*', expand_wildcards='all',
                                        filter_path='metadata.indices.*.state')
            if meta:
                """
                sample result
                {"metadata":{"indices":{"toots_v1_004":{"state":"open"},"toots_v1_002":{"state":"close"}}}}
                """
                for index_name in meta['metadata']['indices'].keys():
                    match = _VERSIONED_INDEX_PATTERN.match(index_name)
                    if match:
                        iv = Version(match.group(2).replace('_', '.'))
                        if iv <= Version(to_version):
                            if iv > found_version:
                                # We use the recent version to undo it to this version
                                found_version = iv
                                found_index = index_name
                        else:
                            if index_alias not in delete_indexes:
                                delete_indexes[index_alias] = [index_name]
                            else:
                                if index_name not in delete_indexes[index_alias]:
                                    delete_indexes[index_alias].append(index_name)

            # We have now the recent index near to the undo-point or None (if the index will be gone)
            keep_indexes[index_alias] = {"undo_index": found_index, "undo_version": found_version}

    if not keep_indexes:
        print(f"No index found to undo to {to_version}. "
              "Maybe older version cleand up or undo does not contain 'create index' commands.")

    # Now we check the current indexes with the alias on it and sort out the keep-indexes with "noop"
    undo_operations = {}
    for keep_alias, keep_index in keep_indexes.items():
        alias_response = client.indices.get(keep_alias, filter_path='*.aliases.*')
        if len(alias_response) == 1:
            remove_index = next(iter(alias_response.keys()))
            match = _VERSIONED_INDEX_PATTERN.match(remove_index)
            if match:
                if Version(match.group(2).replace('_', '.')) > keep_index['undo_version']:
                    keep_index['from_index'] = remove_index
                    undo_operations[keep_alias] = keep_index

    print(f"Run undo on {undo_operations} with delete={delete}")
    if delete and not archive:
        print(f"Delete {delete_indexes} after undo.")
    if archive:
        print(f"Archive {delete_indexes} before undo.")

    # Now we run the undo operations:
    # -------------------------------
    # Everything under the same history record:
    message = "Undo operation:\n"
    # Because the undo-History is not a real file, we change the information:
    undo_point_script['__self']['path'] = "undo://" + os.path.basename(undo_point_script['__self']['path'])
    undo_point_script['__self']['checksum'] = None
    undo_history_id = Migration.create_history(client, undo_point_script, undo_operation=True)
    start_ms = time.time_ns() / 1_000_000

    for undo_alias, undo_op in undo_operations.items():
        undo_dest = undo_op['undo_index']
        message += f"Undo alias {undo_alias} to {undo_dest} = "
        try:
            Migration.switch_version(client, undo_alias, index_to=undo_dest)

            # Check the current alias:
            index_info = client.indices.get(undo_alias, filter_path='*.aliases.*')
            if index_info:
                dest_confirm = next(iter(index_info))
                if dest_confirm == undo_dest:
                    print(f"Undo '{undo_alias}' to '{dest_confirm}' was successful")
                    message += "Ok\n"
                else:
                    message += "Failed. Switch version with unexpected results.\n"
                    Migration.update_history_error(client, history_id=undo_history_id, root_message=message)
                    raise RuntimeError(f"Switch version to undo version {undo_dest} failed")
        except Exception as e:
            message += "Failed.\n"
            Migration.update_history_error(client, undo_history_id, root_cause=e, root_message=message)
            raise

    if delete or archive:
        try:
            for del_alias, del_indexes in delete_indexes.items():
                if archive:
                    message += f"Rename for alias {del_alias}: "
                elif delete:
                    message += f"Delete for alias {del_alias}: "

                for del_index in del_indexes:
                    message += del_index + " "
                    if archive:
                        OpenSearchHelper.os_rename_index(client, del_index, del_index + "-undo",
                                                         close=True, autonumber=True, hide=True)
                    elif delete:
                        client.indices.delete(index=del_index)

                message += " = Ok.\n"
        except Exception as e:
            message += "Failed.\n"
            Migration.update_history_error(client, undo_history_id, root_cause=e, root_message=message)
            raise

    duration = (time.time_ns() / 1_000_000) - start_ms
    Migration.update_history_success(client, duration, undo_history_id, message=message)

    # Finally, the migration history records from the past must be removed
    print(f"Remove versions {undo_versions} from {Migration.MIGRATION_HISTORY}")
    search_all = {
        "size": 100,
        "query": {
            "match_all": {}
        }
    }
    resp = client.search(index=Migration.MIGRATION_HISTORY, body=search_all, scroll='5s')
    scroll_id = resp['_scroll_id']
    while len(resp['hits']['hits']):
        for doc in resp['hits']['hits']:
            history_version = Version(str(doc['_source']['version']))
            if history_version > undo_version:
                client.delete(index=Migration.MIGRATION_HISTORY, id=doc['_id'])

        resp = client.scroll(scroll_id=scroll_id, scroll='5s')
        scroll_id = resp['_scroll_id']

    client.clear_scroll(scroll_id=scroll_id)
