from . import OpenSearchHelper
from . import Migration

from opensearchpy import OpenSearch
from datetime import datetime

"""
A backup helper to copy all well-known indexes to indexes with the name 'bck_YYYY-MMMM-DD-HH-mm_<indexname>'

Examples:

# Backup the default 'migration' data
backup(OpenSearchHelper.os_open(config_yaml="oscluster-config.yaml")) 

# Restore the backup 2022-06-09_16-25-28 to the default migration.
restore(OpenSearchHelper.os_open(config_yaml="oscluster-config.yaml"), name="2022-06-09_16-25-28")
"""


def _list_indexes_from_migration(path="./os/migration") -> [str]:
    """
    Lists all well known indexes.

    :param path: the path to read the migration files to list the indexes to backup later
    :return: a list of indexes, managed by the migration
    """
    indexes = []
    for migration_file in Migration.list_migration_files(dir_name=path):
        migration = Migration.load_migration(dir_name=path, file_name=migration_file)
        for r in migration['run']:
            if r['call'] == 'create index':
                index = r['index_name']
                if index not in indexes:
                    indexes.append(index)
    return indexes


def backup(client: OpenSearch, prefix='bck_', name=None, indexes_from="./os/migration", *indexes: str):
    """
    Runs a backup.
    :param client: OpenSearch client to access the cluster
    :param prefix: Prefix for the backup (default is 'bck_')
    :param name: Name for the backup (default is a timestamp like "%Y-%m-%d_%H-%M-%S"). If the Name contains %ts,
    the %ts will be replaced with the current timestamp.
    :param indexes_from: Loads the index collection from migration scripts
    :param indexes: A list of Indexes to backup
    """
    if not indexes:
        indexes = _list_indexes_from_migration(path=indexes_from)

    ts = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    if not name:
        name = ts
    else:
        name = str(name).replace("%ts", ts)

    for index in indexes:
        backup_name = prefix + name + "_" + index
        if client.indices.exists(index) and not client.indices.exists(backup_name):
            OpenSearchHelper.os_copy_index(client, index_from=index, index_to=backup_name)


def restore(client: OpenSearch, name:str, prefix='bck_', indexes_from="./os/migration", *indexes: str):
    """
    Restores the content form the backup to the corresponding index(-alias) names.
    :param client: OpenSearch cluster client-access
    :param name: a name or date_time string that must match "%Y-%m-%d_%H-%M-%S"
    :param prefix: the prefix of the backups (default is 'bck_')
    :param indexes_from: load the index names from migration scripts
    :param indexes: use this list of indexes
    """
    restore_to_migration = False
    if not indexes:
        indexes = _list_indexes_from_migration(path=indexes_from)
        restore_to_migration = True

    for index in indexes:
        backup_name = prefix + name + "_" + index
        if not client.indices.exists(backup_name):
            print(f"Backup index '{backup_name}' not found. Ignore restore to {index}")
        if not client.indices.exists(index):
            if restore_to_migration:
                print(f"Restore index '{index}' not found. Restore to migration index needs an existing index.")
            else:
                # We restore to new index
                OpenSearchHelper.os_copy_index(client, index_from=backup_name, index_to=index)
        else:
            # We restore to an exting index
            OpenSearchHelper.os_copy_documents(client, index_from=backup_name, index_to=index)
