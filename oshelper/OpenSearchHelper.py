import os

import yaml
from opensearchpy import OpenSearch, RequestError

# ###############################################
# Helper with some high-level OpenSearch-methods
# ###############################################


def os_dev_login (host, port):
    """
    Simple OpenSearch Development_Environment login without authentication and plain communication.
    :param host: host
    :param port: port
    :return: opensearch object to access the single node cluster
    """
    return OpenSearch(
        hosts = [{'host': host, 'port': port}],
        http_compress = True, # enables gzip compression for request bodies
        use_ssl = False
    )


def os_open (config_yaml: str) -> OpenSearch:
    """
    Opens an OpenSearch cluster with the given configuration. Currently, only a dev environment is supported.
    :param config_yaml: A path to a configuration YAML file
    :return: an OpenSearch object to access the cluster via an API
    """
    if not os.path.isabs(config_yaml):
        # First we check the home folder (will override the standard config from repository)
        config_home=os.path.expanduser(os.path.join("~", config_yaml))
        if os.path.exists(config_home):
            config_yaml = config_home

    if not os.path.exists(config_yaml):
        raise ValueError(f"Given path '{config_yaml}' for the config-yaml file does not exist")

    # default settings
    host = '127.0.0.1'
    port = 9200

    with open(config_yaml, 'r') as stream:
        config = yaml.safe_load(stream)

        if not config.get("nodes", None):
            raise ValueError(f"Missing 'nodes' section in the yaml configuration")

        default_node = config["nodes"].get("default", None)
        if not default_node:
            raise ValueError(f"Missing 'nodes.default' section in the yaml configuration")

        host = default_node.get ('host', host)
        port = default_node.get ('port', port)

    return os_dev_login(host, port)


def os_copy_documents(client: OpenSearch, index_from: str, index_to: str, script = None):
    """
    Use the Reindex API to copy all documents from source to destination. The destination index must be empty.
    :param client: OpenSearch client
    :param index_from: source index
    :param index_to: destination index
    :param script: optional script-object to manage the reindex
    :return: reindex response
    """
    reindex = {
        "source": {
            "index": index_from
        },
        "dest": {
            "index": index_to,
            "op_type": "create"
        }
    }

    # Add the script object to the reindex body
    if script:
        if 'script' in script:
            reindex['script'] = script['script']
        else:
            reindex['script'] = script

    response = client.reindex(body=reindex, requests_per_second=10_000, refresh=True)

    print(f"Copied {response['created']} documents from {index_from} to {index_to}")

    if response['failures']:
        print(f"Failures on copy: {response['failures']}")
        raise RuntimeError(f"Failure on copying document from  {index_from} to {index_to}")
    return response


def os_copy_index(client: OpenSearch, index_from: str, index_to: str, script = None):
    """
    Copies an index.

    Hig-Level method.

    :param client: OpenSearch client
    :param index_from: Source index name (can be an alias or real index name)
    :param index_to: Destination name (must be a non-existing index-name)
    :param script: optional script-object to manage the reindex
    """

    index_info = client.indices.get(index=index_from)

    if client.indices.exists(index_to):
        raise ValueError(f'New name {index_to} exists. Rename is not possible for {index_from}.')

    real_name = next(iter(index_info.keys()))

    create_index = {
        'mappings': index_info[real_name]['mappings'],
        'settings': index_info[real_name]['settings']
    }
    # Remove some meta fields:
    create_index['settings']['index'].pop('provided_name')
    create_index['settings']['index'].pop('creation_date')
    create_index['settings']['index'].pop('uuid')
    create_index['settings']['index'].pop('version')

    client.indices.create(index_to, body=create_index)

    try:
        os_copy_documents(client, index_from, index_to, script=script)
    except RequestError as re:
        print(f"HTTP STATUS {re.status_code}")
        if 'error' in re.info:
            for line in re.info['error']['script_stack']:
                print(line)
            print()
            print(re.info['error']['caused_by'])
            print()
            print(re.info['error']['script'])
        raise


def os_rename_index(client: OpenSearch, index_from: str, index_to: str, hide=False, close=False, autonumber=False):
    """
    Renames the index.

    Hig-Level method.

    :param client: OpenSearch client
    :param index_from: Original name
    :param index_to: Destination name
    :param hide: hide with rename
    :param close: Close the renamed index (for archiving functions)
    :param autonumber: renames the dest to a number, if already existing
    :return: new name
    """

    if hide:
        index_to = "." + index_to

    index_info = client.indices.get(index=index_from)
    real_name = next(iter(index_info.keys()))

    # Special case to rename a versioned index to the original alias
    rename_to_alias = len(index_info[index_from]['aliases']) == 1 \
                      and next(iter(index_info[index_from]['aliases'])) == index_to

    if not rename_to_alias:
        if real_name != index_from:
            raise ValueError(f'{index_from} is an alias for {real_name}. Rename is not possible.')
        if len(index_info[index_from]['aliases']):
            raise ValueError(f'{index_from} has aliases. Rename is not possible.')

        if client.indices.exists(index_to):
            if not autonumber:
                raise ValueError(f'New name {index_to} exists. Rename is not possible for {index_from}.')
            else:
                # We search for a number
                num = 2
                index_to_try = index_to + f"-({num})"
                while client.indices.exists(index_to_try):
                    num += 1
                    index_to_try = index_to + f"_({num})"
                index_to = index_to_try

    else:
        # We must remove the alias:
        remove_alias = {
            "actions": [
                {
                    "remove": {
                        "index": index_from,
                        "alias": index_to
                    }
                }
            ]
        }
        client.indices.update_aliases(body=remove_alias)

    os_copy_index(client, index_from, index_to)
    if close:
        client.indices.close(index=index_to, ignore_unavailable=True)

    # No errors, so far so good. We delete the source:
    client.indices.delete(index_from)
    return index_to
