// We simulate the ctx in OpenSearch reindex script context
def ctx = ["_source": ["version": 1.001]]

// Here the real script for OpenSearch starts:
//!script
def normalizeVer (String ver) {
    def pZeros = ~'^0+(?!$)'
    def pDotSplit = ~'\\.'
    if (ver == null || ver.length() == 0) {
        return "0"
    }
    if (ver.startsWith(".")) {
        ver = "0" + ver
    }
    String norm = ""
    for (String v : pDotSplit.split(ver)) {
        if (norm.length() > 0) {
            norm = norm + "."
        }

        if (v=='') {
            norm = norm + "0"
        } else {
            norm = norm + pZeros.matcher(v).replaceFirst('')
        }
    }
    return norm;
}
ctx['_source']['version'] = normalizeVer(ctx['_source']['version'].toString());
//!endscript

// test our single mock
assert "1.1" == ctx['_source']['version']

// Here we can test something else
assert "1.0.1" == normalizeVer("1..1")
assert "0" == normalizeVer("0")
assert "0.3" == normalizeVer(".3")
assert "3.0.2" == normalizeVer("0003.0.02")
assert "3.0.200" == normalizeVer("0003.0.0200")
assert "3.0.200.4" == normalizeVer("0003.0.0200.4")
assert "3.0.200.4" == normalizeVer("0003.0.0200.004")
assert "0.1" == normalizeVer("0.1")
assert "0.1" == normalizeVer("000.0001")
assert "2.1" == normalizeVer("002.1")
assert "0" == normalizeVer("")
assert "0" == normalizeVer(null)
